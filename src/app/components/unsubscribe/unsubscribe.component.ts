import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../../services/user.service';
import {User} from '../../models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.scss']
})
export class UnsubscribeComponent implements OnInit {

  public user: User;
  public checkoutForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {
    this.checkoutForm = formBuilder.group({});
  }

  ngOnInit(): void {
    this.userService.get()
      .subscribe(user => {
        this.user = user;
      });
  }

  public onUnsubscribe(): void {
    this.userService.delete(this.user.id)
      .subscribe(() => {
          this.router.navigateByUrl('/sign_in');
        },
        () => {
          alert('error occurred');
    });
  }
}
