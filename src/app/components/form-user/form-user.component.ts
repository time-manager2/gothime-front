import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Role } from 'src/app/models/user';
import {UserService} from '../../services/user.service';

function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}

@Component({
  selector: 'app-form-user',
  templateUrl: './form-user.component.html',
  styleUrls: ['./form-user.component.scss', '../app/app.component.scss']
})
export class FormUserComponent implements OnInit, OnChanges {
  public userFormGroup: FormGroup;

  @Input() username: string;
  @Input() email: string;
  @Input() roles: Role[];
  @Input() submitButtonName: string;

  @Output() submitForm: EventEmitter<FormGroup> = new EventEmitter();

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {
    this.userFormGroup = this.formBuilder.group({
      email: new FormControl('',  [
        Validators.email,
        Validators.required,
        Validators.minLength(5) // x@x.x
      ]),
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(1)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(1)
      ]),
      passwordConfirmation: new FormControl('',  [
        Validators.required,
        Validators.minLength(1)
      ]),
      roles: new FormControl([])
    }, {
      validators: MustMatch('password', 'passwordConfirmation')
    });
  }

  onSubmit(): void {
    if (!this.userFormGroup.valid) {
      return;
    }
    this.submitForm.emit(this.userFormGroup);
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.userFormGroup.controls.email.setValue(this.email);
    this.userFormGroup.controls.username.setValue(this.username);
    this.userFormGroup.controls.roles.setValue(this.roles);
  }

  ngOnInit(): void {
    this.userFormGroup.controls.email.setValue(this.email);
    this.userFormGroup.controls.username.setValue(this.username);
    this.userFormGroup.controls.roles.setValue(this.roles);
  }
}
