import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../models/user';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss']
})
export class ViewUserComponent implements OnInit {

  @Input() users: User[];
  @Input() redirectUrl: string;

  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  onClickEdit(userId: number): void {
    this.router.navigateByUrl(`/${this.redirectUrl}/${userId}/edit`);
  }

  onClickDelete(userId: number): void {
    this.userService.delete(userId)
      .subscribe(() => {
        location.reload();
      });
  }
}
