import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute} from '@angular/router';
import {User} from '../../../models/user';

@Component({
  selector: 'app-edit-manager-page',
  templateUrl: './edit-manager-page.component.html',
  styleUrls: ['./edit-manager-page.component.scss', '../../app/app.component.scss']
})
export class EditManagerPageComponent implements OnInit {
  public username: string;
  public email: string;
  public targetUserId: string;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.targetUserId = this.route.snapshot.paramMap.get('manager_id');
    this.userService.getUser(this.targetUserId)
      .subscribe((user) => {
        this.username = user.username;
        this.email = user.email;
      });
  }

  submitFormHandler(formGroup: FormGroup): void {
    const data = {...{id: this.targetUserId}, ...formGroup.value};
    this.userService.update(data as User)
      .subscribe(() => {
          location.reload();
        },
        () => {
          alert('error occurred');
        });
  }

}
