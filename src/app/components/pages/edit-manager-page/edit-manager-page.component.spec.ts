import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditManagerPageComponent } from './edit-manager-page.component';

describe('EditManagerPageComponent', () => {
  let component: EditManagerPageComponent;
  let fixture: ComponentFixture<EditManagerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditManagerPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditManagerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
