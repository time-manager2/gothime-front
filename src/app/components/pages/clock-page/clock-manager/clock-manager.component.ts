import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {WorkingTimeService} from '../../../../services/working-time.service';
import {WorkingTime} from '../../../../models/workingtime';
import {User} from '../../../../models/user';

@Component({
  selector: 'app-clock-manager',
  templateUrl: './clock-manager.component.html',
  styleUrls: ['./clock-manager.component.scss']
})
export class ClockManagerComponent implements OnInit {
  @Input() workingTimeList: WorkingTime[];

  // Toggle for refresh (clock out) and clock (clock in) button in form component.
  public clockIn: boolean;
  public user: User;

  constructor(
    private userService: UserService,
    private workingTimeService: WorkingTimeService
  ) {
    // No work period in progress by default. Waiting for working time component info.
    // TODO: get working times.
    this.clockIn = true;
  }

  ngOnInit(): void {
    this.userService.get()
      .subscribe(user => {
        this.user = user;
      });
  }

  clockInChangedHandler(clockIn): void {
    this.clockIn = clockIn;
  }
}
