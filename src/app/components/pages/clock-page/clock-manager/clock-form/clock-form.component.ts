import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from '../../../../../services/user.service';
import {User} from '../../../../../models/user';
import {ClockService} from '../../../../../services/clock.service';

@Component({
  selector: 'app-clock-form',
  templateUrl: './clock-form.component.html',
  styleUrls: ['./clock-form.component.scss']
})
export class ClockFormComponent implements OnInit {
  public user: User;
  public clockIn: boolean;
  public currentTime: Date;
  @Output() clockInChanged: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private userService: UserService,
    private clockManagerService: ClockService
  ) {
    this.clockIn = true;
  }

  onSubmit(): void {
    this.clockManagerService.create({
      time: this.currentTime,
      status: this.clockIn,
      user: this.user.id
    })
      .subscribe(() => {
          this.onClockIn();
        },
        () => {
          console.log('something went wrong');
        });
  }

  onClockIn(): void {
    this.clockIn = !this.clockIn;
    this.clockInChanged.emit(this.clockIn);
  }

  ngOnInit(): void {
    this.userService.get()
      .subscribe(user => {
        this.user = user;
      });
    setInterval(() => {
      this.currentTime = new Date();
    }, 1000);
  }
}
