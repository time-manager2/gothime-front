import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEmployeeDashboardPageComponent } from './view-employee-dashboard-page.component';

describe('ViewEmployeeDashboardPageComponent', () => {
  let component: ViewEmployeeDashboardPageComponent;
  let fixture: ComponentFixture<ViewEmployeeDashboardPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewEmployeeDashboardPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEmployeeDashboardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
