import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewManagersPageComponent } from './view-managers-page.component';

describe('ViewManagersPageComponent', () => {
  let component: ViewManagersPageComponent;
  let fixture: ComponentFixture<ViewManagersPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewManagersPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewManagersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
