import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user';

@Component({
  selector: 'app-view-managers-page',
  templateUrl: './view-managers-page.component.html',
  styleUrls: ['./view-managers-page.component.scss', '../../app/app.component.scss']
})
export class ViewManagersPageComponent implements OnInit {

  public managers: User[];

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getManagers()
      .subscribe((users) => {
        this.managers = users;
      });
  }
}
