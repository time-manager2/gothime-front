import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewEmployeesPageComponent } from './view-employees-page.component';

describe('ViewEmployeesPageComponent', () => {
  let component: ViewEmployeesPageComponent;
  let fixture: ComponentFixture<ViewEmployeesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewEmployeesPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewEmployeesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
