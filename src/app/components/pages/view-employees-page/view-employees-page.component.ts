import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user';

@Component({
  selector: 'app-view-employees-page',
  templateUrl: './view-employees-page.component.html',
  styleUrls: ['./view-employees-page.component.scss', '../../app/app.component.scss']
})
export class ViewEmployeesPageComponent implements OnInit {

  public employees: User[];

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService.getEmployees()
      .subscribe((users) => {
        this.employees = users;
      });
  }
}
