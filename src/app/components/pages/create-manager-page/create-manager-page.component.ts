import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {User} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-manager-page',
  templateUrl: './create-manager-page.component.html',
  styleUrls: ['./create-manager-page.component.scss', '../../app/app.component.scss']
})
export class CreateManagerPageComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  submitFormHandler(formGroup: FormGroup): void {
    this.userService.create(formGroup.value as User)
      .subscribe(() => {
        this.router.navigateByUrl('/managers');
      },
      _ => {
        alert('error occurred');
      });
  }
}
