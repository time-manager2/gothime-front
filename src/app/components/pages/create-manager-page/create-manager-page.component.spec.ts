import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateManagerPageComponent } from './create-manager-page.component';

describe('CreateManagerPageComponent', () => {
  let component: CreateManagerPageComponent;
  let fixture: ComponentFixture<CreateManagerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateManagerPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateManagerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
