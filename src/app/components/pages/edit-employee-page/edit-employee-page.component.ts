import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {User} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-edit-employee-page',
  templateUrl: './edit-employee-page.component.html',
  styleUrls: ['./edit-employee-page.component.scss', '../../app/app.component.scss']
})
export class EditEmployeePageComponent implements OnInit {

  public username: string;
  public email: string;
  public targetUserId: string;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.targetUserId = this.route.snapshot.paramMap.get('employee_id');
    this.userService.getUser(this.targetUserId)
      .subscribe((user) => {
        this.username = user.username;
        this.email = user.email;
      });
  }

  submitFormHandler(formGroup: FormGroup): void {
    const data = {...{id: this.targetUserId}, ...formGroup.value};
    this.userService.update(data as User)
      .subscribe(() => {
          location.reload();
        },
        () => {
          alert('error occurred');
        });
  }

}
