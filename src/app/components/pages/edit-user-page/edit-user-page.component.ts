import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../services/user.service';
import {User} from '../../../models/user';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-edit-user-page',
  templateUrl: './edit-user-page.component.html',
  styleUrls: ['./edit-user-page.component.scss', '../../app/app.component.scss']
})
export class EditUserPageComponent implements OnInit {
  public user: User;

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.userService.get()
      .subscribe(user => {
        this.user = user;
      });
  }

  submitFormHandler(formGroup: FormGroup): void {
    const data = {...this.user, ...formGroup.value};
    this.userService.update(data as User)
      .subscribe(() => {
          location.reload();
        },
        () => {
          alert('error occurred');
        });
  }
}
