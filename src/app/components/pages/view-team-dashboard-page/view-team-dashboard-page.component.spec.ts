import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTeamDashboardPageComponent } from './view-team-dashboard-page.component';

describe('ViewTeamDashboardPageComponent', () => {
  let component: ViewTeamDashboardPageComponent;
  let fixture: ComponentFixture<ViewTeamDashboardPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewTeamDashboardPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTeamDashboardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
