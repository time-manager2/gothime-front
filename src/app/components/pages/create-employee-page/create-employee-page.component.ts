import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {User} from '../../../models/user';
import {UserService} from '../../../services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-create-employee-page',
  templateUrl: './create-employee-page.component.html',
  styleUrls: ['./create-employee-page.component.scss', '../../app/app.component.scss']
})
export class CreateEmployeePageComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  submitFormHandler(formGroup: FormGroup): void {
    this.userService.create(formGroup.value as User)
      .subscribe(_ => {
        this.router.navigateByUrl('/employees');
      },
      _ => {
        alert('error occurred');
      });
  }
}
