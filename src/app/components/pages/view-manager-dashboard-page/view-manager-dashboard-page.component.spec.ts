import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewManagerDashboardPageComponent } from './view-manager-dashboard-page.component';

describe('ViewManagerDashboardPageComponent', () => {
  let component: ViewManagerDashboardPageComponent;
  let fixture: ComponentFixture<ViewManagerDashboardPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewManagerDashboardPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewManagerDashboardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
