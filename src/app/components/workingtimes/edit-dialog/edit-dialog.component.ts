import { Component, Inject, OnInit } from '@angular/core';
import { WorkingTime } from 'src/app/models/workingtime';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';

function getStartingDate(form: FormGroup, checkError?: boolean) {
  const values = form.value;
  if (checkError === true && !form.controls.startDate.valid) {
    return form.controls.startDate.value;
  }
  if (form.controls.startHours.valid) {
    values.startDate.set('hours', values.startHours);
  }
  if (form.controls.startMinutes.valid) {
    values.startDate.set('minutes', values.startMinutes);
  }
  return values.startDate.toDate();
}

function getEndingDate(form: FormGroup, checkError?: boolean) {
  const values = form.value;
  if (checkError === true && !form.controls.endDate.valid) {
    return form.controls.endDate.value;
  }
  if (form.controls.endHours.valid) {
    values.endDate.set('hours', values.endHours);
  }
  if (form.controls.endMinutes.valid) {
    values.endDate.set('minutes', values.endMinutes);
  }
  return values.endDate.toDate();
}

function dateRange() {
  return (formGroup: FormGroup) => {
    if (!formGroup.controls.startHours.valid || !formGroup.controls.startMinutes.valid ||
      !formGroup.controls.endHours.valid || !formGroup.controls.endMinutes.valid) {
        return;
      }

    const startDate = getStartingDate(formGroup, false);
    const endDate = getEndingDate(formGroup, false);

    if (startDate >= endDate) {
      formGroup.controls.startDate.setErrors({ dateRangeInvalid: true });
      formGroup.controls.endDate.setErrors({ dateRangeInvalid: true });
    } else {
      formGroup.controls.startDate.setErrors(null);
      formGroup.controls.endDate.setErrors(null);
    }
  }
}

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.scss']
})
export class EditDialogComponent implements OnInit {

  public workingtime: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { type: string, workingTime: WorkingTime },
    private fb: FormBuilder
  )
  {
    this.workingtime = fb.group({
      startDate: new FormControl('',  [
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ]),
      startHours: new FormControl('',  [
        Validators.required,
        Validators.min(0),
        Validators.max(23)
      ]),
      startMinutes: new FormControl('',  [
        Validators.required,
        Validators.min(0),
        Validators.max(59)
      ]),
      endDate: new FormControl('',  [
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ]),
      endHours: new FormControl('',  [
        Validators.required,
        Validators.min(0),
        Validators.max(23)
      ]),
      endMinutes: new FormControl('',  [
        Validators.required,
        Validators.min(0),
        Validators.max(59)
      ])
    }, {
      validators: dateRange()
    });
  }

  public getStartingDate(): Date {
    return getStartingDate(this.workingtime, true);
  }

  public getEndingDate(): Date {
    return getEndingDate(this.workingtime, true);
  }

  private getWorkingTime(): WorkingTime {
    return {
      id: this.data.workingTime.id ? this.data.workingTime.id : undefined,
      start: getStartingDate(this.workingtime),
      end: getEndingDate(this.workingtime),
      user: this.data.workingTime.user
    } as WorkingTime;
  }

  public onEdit(): void {
    const res = this.getWorkingTime();
    this.dialogRef.close({
      action: 'edit',
      data: res
    });
  }

  public onDelete(): void {
    this.dialogRef.close({
      action: 'delete',
      data: this.data.workingTime
    });
  }

  public onCreate(): void {
    const res = this.getWorkingTime();
    this.dialogRef.close({
      action: 'create',
      data: res
    });
  }

  public ngOnInit(): void {
    this.workingtime.controls.startDate.setValue(this.data.workingTime.start ? moment(this.data.workingTime.start) : '');
    this.workingtime.controls.endDate.setValue(this.data.workingTime.end ? moment(this.data.workingTime.end) : '');

    this.workingtime.controls.startHours.setValue(this.data.workingTime.start ? this.data.workingTime.start.getHours() : '');
    this.workingtime.controls.endHours.setValue(this.data.workingTime.end ? this.data.workingTime.end.getHours() : '');

    this.workingtime.controls.startMinutes.setValue(this.data.workingTime.start ? this.data.workingTime.start.getMinutes() : '');
    this.workingtime.controls.endMinutes.setValue(this.data.workingTime.end ? this.data.workingTime.end.getMinutes() : '');
  }
}
