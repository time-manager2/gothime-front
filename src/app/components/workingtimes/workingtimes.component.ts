import { AfterViewInit, ChangeDetectorRef, Component, Input, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user';
import { WorkingTime } from 'src/app/models/workingtime';
import * as Highcharts from 'highcharts';
import * as moment from 'moment';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { EditDialogComponent } from './edit-dialog/edit-dialog.component';
import { WorkingTimeService } from 'src/app/services/working-time.service';
import { filter } from 'rxjs/operators';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-workingtimes',
  templateUrl: './workingtimes.component.html',
  styleUrls: ['./workingtimes.component.scss']
})
export class WorkingtimesComponent implements AfterViewInit {

  @Input('user') public user: User;
  public dataLoaded = false;
  public workingTimes: WorkingTime[] = [];
  private chart: Highcharts.Chart = null;
  public options: Highcharts.Options = {};
  private minBound = moment().locale('fr').startOf('week').toDate().getTime();
  private maxBound = moment().locale('fr').endOf('week').toDate().getTime();
  private displayFormat = 'week'
  private currentWorkingTime = new BehaviorSubject<WorkingTime>(null);
  private currentWorkingTimeObservable: Observable<WorkingTime> = this.currentWorkingTime.asObservable();

  constructor(
    private dialog: MatDialog,
    private cdr: ChangeDetectorRef,
    private _workingTimeService: WorkingTimeService
  ) {

  }

  public getFormatedTitle(): string {
    if (this.displayFormat === 'week') {
      return moment(this.minBound).format('[Week] w - YYYY');
    } else if (this.displayFormat === 'month') {
      return moment(this.minBound).format('MMMM - YYYY');
    } else {
      return moment(this.minBound).format('MMMM DD - YYYY');
    }
  }

  private initChart(): void {
    this.options = {
      chart: {
        type: 'columnrange',
        inverted: true,
      },
      title: {
        text: ''
      },
      xAxis: {
        visible: false
      },
      yAxis: {
        type: 'datetime',
        title: {
          text: ''
        },
        min: this.minBound,
        max: this.maxBound,
        startOnTick: false,
        endOnTick: false,
        minTickInterval: 24 * 3600 * 1000,
        labels: {
          formatter: function() {
            return Highcharts.dateFormat('%A %e', this.value);
          }
        }
      },
      plotOptions: {
        columnrange: {
          borderRadius: 7
        },
        series: {
          events: {
            click: function () {
              (this.name as any).observable.next(this.name);
            }
          }
        }
      },
      tooltip: {
        formatter: function() {
          const range = this.point as any;
          const low = range.low - 1000 * 3600;
          const high = range.high - 1000 * 3600;
          return moment(low).format('DD/MM/yyyy HH:mm') + ' - ' + moment(high).format('DD/MM/yyyy HH:mm');
        }
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      series: []
    };
  }

  // Display only visible working times
  private getVisibleWorkingTimes(workingTimes: WorkingTime[]): WorkingTime[] {
    return workingTimes.filter(workingTime => {
      return workingTime.end.getTime() > this.minBound && this.maxBound > workingTime.start.getTime();
    });
  }

  public onWeekForward(): void {
    this.minBound = moment(this.minBound).add(1, this.displayFormat as any).toDate().getTime();
    this.maxBound = moment(this.minBound).endOf(this.displayFormat as any).toDate().getTime();
    this.chart.update({
      yAxis: {
        min: this.minBound + 1000 * 3600,
        max: this.maxBound + 1000 * 3600
      }
    });
    this.chartUpdateWorkingTimes(this.getVisibleWorkingTimes(this.workingTimes));
  }

  public onWeekBackward(): void {
    this.minBound = moment(this.minBound).subtract(1, this.displayFormat as any).toDate().getTime();
    this.maxBound = moment(this.minBound).endOf(this.displayFormat as any).toDate().getTime();
    this.chart.update({
      yAxis: {
        min: this.minBound + 1000 * 3600,
        max: this.maxBound + 1000 * 3600
      }
    });
    this.chartUpdateWorkingTimes(this.getVisibleWorkingTimes(this.workingTimes));
  }

  private setChartVisible(): void {
    this.chart = Highcharts.chart('container', this.options);
  }

  private chartUpdateWorkingTimes(workingtimes: WorkingTime[]): void {
    this.options.series = workingtimes.map(workingtime => ({
      name: {
        ...workingtime,
        observable: this.currentWorkingTime
      },
      type: 'columnrange',
      data: [[workingtime.start.getTime() + 1000 * 3600, workingtime.end.getTime() + 1000 * 3600]],
      color: workingtime.color,
      animation: false
    })) as any;
    this.setChartVisible();
  }

  private loadData() {
    this.dataLoaded = false;
    this.cdr.detectChanges();

    this.initChart();
    this.chartUpdateWorkingTimes(this.getVisibleWorkingTimes(this.workingTimes));

    this.dataLoaded = true;
    this.cdr.detectChanges();
  }

  ngAfterViewInit() {
    this.currentWorkingTimeObservable
      .subscribe(workingTime => {
        if (!workingTime) {
          return;
        }
        this.onEdit(workingTime);
      });
      this.fetch();
  }

  public fetch() {
    this._workingTimeService.getByUserAll(this.user)
      .subscribe(res => {
        this.workingTimes = res;
        this.loadData();
      });
  }

  public onEdit(workingTime: WorkingTime): void {
    const dialog = this.dialog.open(EditDialogComponent, {
      data: {
        workingTime,
        type: 'Edit'
      },
      maxWidth: "100%",
      minWidth: "0%"
    });
    dialog.afterClosed()
      .subscribe(res => {
        if (!res) {
          return;
        }
        if (res.action === 'edit') {
          this._workingTimeService.update(res.data)
            .subscribe(() => {
              this.fetch();
            });
        } else if (res.action === 'delete') {
          this._workingTimeService.delete(res.data.id)
            .subscribe(() => {
              this.fetch();
            });
        }
      })
  }

  public displayToogle(event): void {
    const value = event.value as string;
    this.minBound = moment(this.minBound).locale('fr').startOf(value as any).toDate().getTime();
    this.maxBound = moment(this.minBound).locale('fr').endOf(value as any).toDate().getTime();
    this.displayFormat = value;
    let minTickInterval;
    let dateFormat;

    if (this.displayFormat === 'week') {
      minTickInterval = 24 * 3600 * 1000;
      dateFormat = '%A %e';
    } else if (this.displayFormat === 'month') {
      minTickInterval = 24 * 3600 * 1000;
      dateFormat = '%d';
    } else if (this.displayFormat === 'day'){
      minTickInterval = 3600 * 1000;
      dateFormat = '%Hh';
    }

    this.chart.update({
      yAxis: {
        min: this.minBound + 1000 * 3600,
        max: this.maxBound + 1000 * 3600,
        minTickInterval,
        labels: {
          formatter: function() {
            return Highcharts.dateFormat(dateFormat, this.value);
          }
        }
      }
    });
    this.chartUpdateWorkingTimes(this.getVisibleWorkingTimes(this.workingTimes));
  }

  public onCreate(): void {
    const dialog = this.dialog.open(EditDialogComponent, {
      data: {
        workingTime: {
          user: this.user.id
        },
        type: 'Create'
      },
      maxWidth: "100%",
      minWidth: "0%"
    });
    dialog.afterClosed()
      .subscribe(res => {
        if (!res) {
          return;
        }
        if (res.action === 'create') {
          this._workingTimeService.create(res.data)
            .subscribe(res => {
              this.fetch();
            });
        }
      })
  }
}
