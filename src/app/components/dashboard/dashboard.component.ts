import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { PwaUpdateService } from 'src/app/services/pwa-update.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public user: User = null;

  constructor(private _userService: UserService) {

  }

  ngOnInit(): void {
    this._userService.get()
      .subscribe(user => {
        this.user = user;
        if (!user) {
          return;
        }
      });
  }
}
