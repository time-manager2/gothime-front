import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  public credentials: FormGroup;
  public loginError = null;
  public alreadyLogged = false;

  constructor(
      private fb: FormBuilder,
      private _userService: UserService,
      private router: Router
    ) {
    this.credentials = fb.group({
      email: new FormControl('',  [
        Validators.email,
        Validators.required,
        Validators.minLength(1)
      ]),
      password: new FormControl('',  [
        Validators.required,
        Validators.minLength(1)
      ])
    });
  }

  public onSignin(): void {
    if (!this.credentials.valid) {
      return;
    }
    this._userService.login(this.credentials.value as User);
  }

  public ngOnInit() {
     this._userService.get()
      .subscribe(user => {
        if (!user) {
          return;
        }
        if (this.alreadyLogged === false) {
          this.router.navigateByUrl('/dashboard');
          this.alreadyLogged = true;
        }
    });
  }
}
