import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { PwaUpdateService } from 'src/app/services/pwa-update.service';
import { RouterEventService } from 'src/app/services/router-event.service';
import { UserService } from 'src/app/services/user.service';

function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  }
}

@Component({
  selector: 'user-info-dialog',
  templateUrl: './user-info-dialog.html',
  styleUrls: ['./user-info-dialog.scss']
})
export class UserInfoDialog {

  public credentials: FormGroup;

  constructor(
      public dialogRef: MatDialogRef<UserInfoDialog>,
      @Inject(MAT_DIALOG_DATA) public data: User,
      private fb: FormBuilder,
      private _userService: UserService,
    ) {
    this.credentials = fb.group({
      password: new FormControl('',  [
        Validators.required,
        Validators.minLength(1)
      ]),
      passwordConfirmation: new FormControl('',  [
        Validators.required,
        Validators.minLength(1)
      ])
    }, {
      validators: MustMatch('password', 'passwordConfirmation')
    });
  }


  public onNoClick(): void {
    this.dialogRef.close();
  }

  public onPasswordChange(): void {
    if (!this.credentials.valid) {
      return;
    }
    this.data.password = this.credentials.controls.password.value;
    this._userService.update(this.data)
      .subscribe(_ => {
        this.dialogRef.close();
      });
  }
}

@Component({
  selector: 'update-dialog',
  templateUrl: './update-dialog.html'
})
export class UpdateDialog {

  constructor(public dialogRef: MatDialogRef<UpdateDialog>) {
    dialogRef.disableClose = true;
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public user: User = null;
  public pages = [
    {
      name: 'ABOUT ME',
      pages: [
        { name: 'Dashboard', icon: 'dashboard', link: 'dashboard' },
        { name: 'Edit', icon: 'edit', link: 'edit' },
        { name: 'Clock', icon: 'schedule', link: 'clock' }
      ]
    },
    {
      name: 'MY EMPLOYEES',
      pages: [
        { name: 'Show', icon: 'group', link: 'employees' },
        { name: 'Create', icon: 'person_add', link: 'employee/create'}
      ]
    },
    {
      name: 'MY MANAGERS',
      pages: [
        { name: 'Show', icon: 'group', link: 'managers' },
        { name: 'Create', icon: 'person_add', link: 'manager/create'}
      ]
    }
  ];

  constructor(
      private _userService: UserService,
      private router: Router,
      private userDialog: MatDialog,
      private _updateService: PwaUpdateService,
      private _snackBar: MatSnackBar,
      private _routerEventService: RouterEventService
    ) {
  }

  public ngOnInit(): void {
    this._userService.get()
      .subscribe(user => {
        this.user = user;
      });
    this._updateService.checkForUpdate();
    this._updateService.onUpdateDetected()
      .subscribe(() => {
        const updateDialog = this.userDialog.open(UpdateDialog, {
          maxWidth: "100%",
          minWidth: "0%"
        });
        updateDialog.afterClosed()
          .subscribe(_ => {
            this._updateService.doUpdate();
          });
      });
    this._updateService.onUpdateDone()
      .subscribe(() => {
        this._snackBar.open('Update done', '', {
          duration: 2000
        });
      });
  }

  public onTitleClick(): void {
    if (this.user) {
      this.router.navigateByUrl('/dashboard');
    } else {
      this.router.navigateByUrl('/sign_in');
    }
  }

  public onLogout(): void {
    this._userService.logout();
    this.router.navigateByUrl('/sign_in');
  }

  public onUserInfo(): void {
    this.userDialog.open(UserInfoDialog, {
      data: this.user,
      maxWidth: "100%",
      minWidth: "0%"
    });
  }
}
