import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {Role, User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private currentUser: BehaviorSubject<User>;
  private readonly currentUserObservable: Observable<User>;

  constructor(
      private _http: HttpClient,
      private router: Router
    ) {
    const userString = localStorage.getItem('userLogin');
    const defaultUser = userString ? JSON.parse(userString).user as User : null;
    this.currentUser = new BehaviorSubject<User>(defaultUser);
    this.currentUserObservable = this.currentUser.asObservable();
  }

  public reloadCurrentUser() {
    return this._http.get<User>(`/user/me`)
      .toPromise()
      .then(user => {
        const userLogin = JSON.parse(localStorage.getItem('userLogin')) as {token: {type: string, value: string}, user: User};
        if (userLogin) {
          userLogin.user = user;
        }
        localStorage.setItem('userLogin', JSON.stringify(userLogin));
        this.currentUser.next(user);
      })
      .catch(() => this.logout())
  }

  public logout(): void {
    localStorage.removeItem('userLogin');
    this.router.navigateByUrl('/sign_in');
    this.currentUser.next(null);
  }

  public login(user: User): void {
    this._http.get<{token: {type: string, value: string}, user: User}>(`/user/login`, {
      params: {
        email: user.email,
        password: user.password
      }
    })
      .subscribe(userLogin => {
        localStorage.setItem('userLogin', JSON.stringify(userLogin));
        this.currentUser.next(userLogin.user);
        this.router.navigateByUrl('/dashboard');
      },
      error => console.log(error))
  }

  public get(): Observable<User> {
    return this.currentUserObservable;
  }

  public update(user: User): Observable<User> {
    return this._http.put<User>(`/user/${user.id}`, user);
  }

  public delete(id: number): Observable<{id: number}> {
    return this._http.delete<{id: number}>(`/user/${id}`);
  }

  public create(user: User): Observable<User> {
    return this._http.post<User>(`/user`, user);
  }

  public signup(user: User): Observable<User> {
    return this._http.post<User>(`/user/signup`, user);
  }

  public getEmployees(): Observable<User[]> {
    return this._http.get<User[]>(`/user/employees`);
  }

  public getManagers(): Observable<User[]> {
    return this._http.get<User[]>(`/user/managers`);
  }

  public setRole(user: User, roleId: number): Observable<User> {
    return this._http.put<User>(`/user/${user.id}/role/${roleId}`, '');
  }

  public getUser(userId: string): Observable<User> {
    return this._http.get<User>(`/user/${userId}`);
  }
}
