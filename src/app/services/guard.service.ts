import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { User } from '../models/user';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private user: User = null;

  constructor(
    private _userService: UserService,
    private router: Router
  ) {
    this._userService.get()
      .subscribe(user => {
        this.user = user
      })
  }

  public async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.user) {
      this.router.navigateByUrl('/sign_in');
      return false;
    } else {
      return true;
    }
  }
}
