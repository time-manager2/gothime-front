import { Injectable } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';
import { filter } from 'rxjs/operators';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class RouterEventService {

  constructor(private _router: Router, private _userService: UserService) {
    _router.events.pipe(
      filter((e): e is RouterEvent => e instanceof RouterEvent)
   ).subscribe((e: any) => {
     if (e.navigationTrigger !== "imperative")
      return;
     this._userService.reloadCurrentUser();
   });
  }
}
