import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Clock} from '../models/clock';
import {Observable} from 'rxjs';
// @ts-ignore
import moment = require('moment');

@Injectable({
  providedIn: 'root'
})
export class ClockService {
  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient) { }

  static formatDate(date: Date): string {
    return moment(date).format('YYYY-MM-DD HH:mm:ss');
  }

  public getByUserId(clock: Clock): Observable<Clock[]> {
    return this._http.get<Clock[]>(`/clocks/${clock.user}`, {
      params: {
        time: ClockService.formatDate(clock.time)
      }
    });
  }

  public create(clock: Clock): Observable<Clock> {
    return this._http.post<Clock>(`/clocks/${clock.user}`, clock);
  }
}
