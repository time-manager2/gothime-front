import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WorkingTime} from '../models/workingtime';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '../models/user';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
// @ts-ignore
export class WorkingTimeService {
  // tslint:disable-next-line:variable-name
  constructor(private _http: HttpClient) {

  }

  private formatDate(date: Date): string {
    return moment(date).format('YYYY-MM-DD HH:mm:ss');
  }

  private parseDate(str: string): Date {
    return moment(str).toDate();
  }

  public getByUserAll(user: User): Observable<WorkingTime[]> {
    return this._http.get<WorkingTime[]>(`/workingtimes/${user.id}`)
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(workingTime => {
          return workingTime.map(obj => {
            obj.start = this.parseDate(obj.start as any);
            obj.end = this.parseDate(obj.end as any);
            return obj;
          });
        })
      );
  }

  public getByUser(workingTime: WorkingTime): Observable<WorkingTime[]> {
    return this._http.get<WorkingTime[]>(`/workingtimes/${workingTime.user}`, {
      params: {
        start: this.formatDate(workingTime.start),
        end: this.formatDate(workingTime.end)
      }
    })
      .pipe(
        // tslint:disable-next-line:no-shadowed-variable
        map(workingTime => {
          return workingTime.map(obj => {
            obj.start = this.parseDate(obj.start as any);
            obj.end = this.parseDate(obj.end as any);
            return obj;
          });
        })
      );
  }

  public getWorkingTimeById(user: User, id: number): Observable<WorkingTime> {
    return this._http.get<WorkingTime>(`/workingtimes/${user.id}/${id}`);
  }

  public update(workingtime: WorkingTime): Observable<WorkingTime> {
    return this._http.put<WorkingTime>(`/workingtimes/${workingtime.id}`, {
      ...workingtime,
      start: this.formatDate(workingtime.start),
      end: this.formatDate(workingtime.end)
    });
  }

  public delete(id: number): Observable<{ id: number }> {
    return this._http.delete<{ id: number }>(`/workingtimes/${id}`);
  }

  public create(workingTime: WorkingTime): Observable<WorkingTime> {
    return this._http.post<WorkingTime>(`/workingtimes/${workingTime.user}`, {
      ...workingTime,
      start: this.formatDate(workingTime.start),
      end: this.formatDate(workingTime.end)
    });
  }

}
