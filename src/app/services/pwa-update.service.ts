import { ApplicationRef, Injectable } from '@angular/core';
import { SwUpdate, UpdateActivatedEvent, UpdateAvailableEvent } from '@angular/service-worker';
import { concat, interval, Observable } from 'rxjs';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PwaUpdateService {

  constructor(private appRef: ApplicationRef, private updates: SwUpdate) {
    const appIsStable$ = appRef.isStable.pipe(first(isStable => isStable === true));
    const everyOneHours$ = interval(1 * 60 * 60 * 1000);
    const everyOneHoursOnceAppIsStable$ = concat(appIsStable$, everyOneHours$);

    everyOneHoursOnceAppIsStable$
      .subscribe(() => {
        this.checkForUpdate();
      });
  }

  public checkForUpdate(): void {
    this.updates.checkForUpdate().catch(_ => {});
  }

  public onUpdateDetected(): Observable<UpdateAvailableEvent> {
    return this.updates.available;
  }

  public onUpdateDone(): Observable<UpdateActivatedEvent> {
    return this.updates.activated;
  }

  public doUpdate() {
    this.updates.activateUpdate()
      .then(() => {
        document.location.reload();
      })
  }
}
