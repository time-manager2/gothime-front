import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClockPageComponent } from './components/pages/clock-page/clock-page.component';
import { SigninPageComponent } from './components/pages/signin-page/signin-page.component';
import { UnsubscribePageComponent } from './components/pages/unsubscribe-page/unsubscribe-page.component';
import {DashboardPageComponent} from './components/pages/dashboard-page/dashboard-page.component';
import {ViewEmployeeDashboardPageComponent} from './components/pages/view-employee-dashboard-page/view-employee-dashboard-page.component';
import {ViewManagerDashboardPageComponent} from './components/pages/view-manager-dashboard-page/view-manager-dashboard-page.component';
import {TeamManagerPageComponent} from './components/pages/team-manager-page/team-manager-page.component';
import {EditEmployeePageComponent} from './components/pages/edit-employee-page/edit-employee-page.component';
import {EditManagerPageComponent} from './components/pages/edit-manager-page/edit-manager-page.component';
import {CreateEmployeePageComponent} from './components/pages/create-employee-page/create-employee-page.component';
import {CreateManagerPageComponent} from './components/pages/create-manager-page/create-manager-page.component';
import {ViewTeamDashboardPageComponent} from './components/pages/view-team-dashboard-page/view-team-dashboard-page.component';
import { AuthGuard } from './services/guard.service';
import {ViewManagersPageComponent} from './components/pages/view-managers-page/view-managers-page.component';
import {ViewEmployeesPageComponent} from './components/pages/view-employees-page/view-employees-page.component';
import {EditUserPageComponent} from './components/pages/edit-user-page/edit-user-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'sign_in', pathMatch: 'full' },
  { path: 'sign_in', component: SigninPageComponent },
  { path: 'edit', component: EditUserPageComponent, canActivate: [AuthGuard] },
  { path: 'unsubscribe', component: UnsubscribePageComponent, canActivate: [AuthGuard] },
  { path: 'clock', component: ClockPageComponent, canActivate: [AuthGuard] },
  { path: 'dashboard', component: DashboardPageComponent, canActivate: [AuthGuard] },
  { path: 'team_manager', component: TeamManagerPageComponent, canActivate: [AuthGuard] },
  {
    path: 'employees',
    component: ViewEmployeesPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employee/create',
    component: CreateEmployeePageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employee/:employee_id/dashboard',
    component: ViewEmployeeDashboardPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'employee/:employee_id/edit',
    component: EditEmployeePageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'managers',
    component: ViewManagersPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'manager/create',
    component: CreateManagerPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'manager/:manager_id/dashboard',
    component: ViewManagerDashboardPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'manager/:manager_id/edit',
    component: EditManagerPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'team/:team_id/dashboard',
    component: ViewTeamDashboardPageComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
