export interface WorkingTime {
  id?: number;
  start: Date;
  end: Date;
  user: number;
  color?: string;
}
