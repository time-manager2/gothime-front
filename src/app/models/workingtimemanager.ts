export interface WorkingTimeManager {
  id?: number;
  start: Date;
  end: Date;
  user: number;
}
