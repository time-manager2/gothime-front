export interface Permission {
  id?: number;
  name: string;
  display_name: string;
}

export interface Role {
  id?: number;
  name: string;
  display_name?: string;
  permissions?: Permission[]
}

export interface User {
  id?: number;
  username: string;
  email: string;
  password?: string;
  roles: Role[];
};
