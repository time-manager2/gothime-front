import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

import { environment } from '../environments/environment';
import { UserService } from './services/user.service';
import { User } from './models/user';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

    private user: User = null;

    constructor(private _userService: UserService) {
        this._userService.get()
            .subscribe(user => {
                this.user = user;
            });
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let options = {
            url: `${environment.baseUrl}${request.url}`
        };
        if (this.user) {
            const userLogin = JSON.parse(localStorage.getItem('userLogin')) as {token: {type: string, value: string}, user: User};
            if (userLogin) {
                options['setHeaders'] = {
                    Authorization: `${userLogin.token.type} ${userLogin.token.value}`
                }
            }
        }
        request = request.clone(options);
        return next.handle(request)
    }
}
