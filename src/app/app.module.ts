import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent, UpdateDialog, UserInfoDialog } from './components/app/app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiInterceptor } from './api.interceptor';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatChipsModule } from '@angular/material/chips';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSortModule } from '@angular/material/sort'
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SigninComponent } from './components/signin/signin.component';
import { ClockPageComponent } from './components/pages/clock-page/clock-page.component';
import { ClockManagerComponent } from './components/pages/clock-page/clock-manager/clock-manager.component';
import { ClockFormComponent } from './components/pages/clock-page/clock-manager/clock-form/clock-form.component';
import { SigninPageComponent } from './components/pages/signin-page/signin-page.component';
import { UnsubscribePageComponent } from './components/pages/unsubscribe-page/unsubscribe-page.component';
import { UnsubscribeComponent } from './components/unsubscribe/unsubscribe.component';
import { DashboardPageComponent } from './components/pages/dashboard-page/dashboard-page.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ViewEmployeeDashboardPageComponent } from './components/pages/view-employee-dashboard-page/view-employee-dashboard-page.component';
import { ViewManagerDashboardPageComponent } from './components/pages/view-manager-dashboard-page/view-manager-dashboard-page.component';
import { TeamManagerPageComponent } from './components/pages/team-manager-page/team-manager-page.component';
import { TeamManagerComponent } from './components/team-manager/team-manager.component';
import { EditEmployeePageComponent } from './components/pages/edit-employee-page/edit-employee-page.component';
import { EditManagerPageComponent } from './components/pages/edit-manager-page/edit-manager-page.component';
import { CreateManagerPageComponent } from './components/pages/create-manager-page/create-manager-page.component';
import { CreateEmployeePageComponent } from './components/pages/create-employee-page/create-employee-page.component';
import { ViewTeamDashboardPageComponent } from './components/pages/view-team-dashboard-page/view-team-dashboard-page.component';
import { FormUserComponent } from './components/form-user/form-user.component';
import { RouterModule } from '@angular/router';
import { ViewManagersPageComponent } from './components/pages/view-managers-page/view-managers-page.component';
import { ViewEmployeesPageComponent } from './components/pages/view-employees-page/view-employees-page.component';
import { ViewUserComponent } from './components/view-user/view-user.component';
import { EditUserPageComponent } from './components/pages/edit-user-page/edit-user-page.component';
import { WorkingtimesComponent } from './components/workingtimes/workingtimes.component';
import { EditDialogComponent } from './components/workingtimes/edit-dialog/edit-dialog.component';
import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS, MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [
    AppComponent,
    UserInfoDialog,
    UpdateDialog,
    SigninComponent,
    ClockPageComponent,
    ClockManagerComponent,
    ClockFormComponent,
    SigninPageComponent,
    UnsubscribePageComponent,
    UnsubscribeComponent,
    DashboardPageComponent,
    DashboardComponent,
    ViewEmployeeDashboardPageComponent,
    ViewManagerDashboardPageComponent,
    TeamManagerPageComponent,
    TeamManagerComponent,
    EditUserPageComponent,
    EditEmployeePageComponent,
    EditManagerPageComponent,
    CreateManagerPageComponent,
    CreateEmployeePageComponent,
    ViewTeamDashboardPageComponent,
    FormUserComponent,
    ViewManagersPageComponent,
    ViewEmployeesPageComponent,
    ViewUserComponent,
    WorkingtimesComponent,
    EditDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production
    }),
    BrowserAnimationsModule,
    HttpClientModule,
    MatButtonModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatInputModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatChipsModule,
    RouterModule,
    MatSnackBarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatGridListModule,
    FlexLayoutModule,
    MatButtonToggleModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: ApiInterceptor,
    multi: true
  },{
    provide: MAT_DATE_LOCALE,
    useValue: 'en_FR'
  },
  {
    provide: MAT_DATE_FORMATS,
    useValue: {
      parse: {
        dateInput: 'LL'
      },
      display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'YYYY'
      }
    }
  }],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {

}
