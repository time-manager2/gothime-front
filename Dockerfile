FROM node:15.0.1-alpine3.10 AS build

WORKDIR /usr/src/app

COPY package.json package-lock.json ./

RUN npm i

COPY . .

ENV PATH $PATH:/usr/src/app/node_modules/.bin

RUN ng build --prod

FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf

COPY --from=build /usr/src/app/dist/gothime-front /usr/share/nginx/html
